import { Component } from '@angular/core';
import { PrintService } from './print.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-print';

  constructor(public printService: PrintService) { }

  onPrintInvoice() {
    const invoiceIds = ['1', '2'];
    this.printService.printDocument('invoice', invoiceIds);
  }

  onPrintLetter() {
    const lettersIds = [];
    this.printService.printDocument('letter', lettersIds);
  }
}
