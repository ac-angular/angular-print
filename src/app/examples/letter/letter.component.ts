import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PrintService } from 'src/app/print.service';

@Component({
  selector: 'app-letter',
  templateUrl: './letter.component.html',
  styleUrls: ['./letter.component.css']
})
export class LetterComponent implements OnInit {

  fieldLettersIds = 'lettersIds';
  lettersIds: string[];
  lettersDetails: Promise<any>[];

  constructor(route: ActivatedRoute, private printService: PrintService) {
    this.lettersIds = route.snapshot.params[this.fieldLettersIds].split(',');
  }

  ngOnInit() {
    // Populamos las promesas con los detalles asociados a cada id
    this.lettersDetails = this.lettersIds.map(id => this.getInvoiceDetails(id));
    // Usamos las promesas para llamar al servicio de impresion
    Promise.all(this.lettersDetails).then(() => this.printService.onDataReady());
  }

  getInvoiceDetails(letterId) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({amount}), 1000)
    );
  }

}
